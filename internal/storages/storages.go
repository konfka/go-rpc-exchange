package storages

import (
	"gitlab.com/konfka/go-rpc-exchange/internal/db/adapter"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/storage"
)

type Storages struct {
	Worker storage.Workerer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Worker: storage.NewWorkerStorage(sqlAdapter),
	}
}
