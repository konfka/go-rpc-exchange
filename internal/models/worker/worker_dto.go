package worker

//go:generate easytags $GOFILE
type WorkerDTO struct {
	ID        int    `json:"id"  db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name      string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	BuyPrice  string `json:"buy_price" db:"buy_price" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	SellPrice string `json:"sell_price" db:"sell_price" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	LastTrade string `json:"last_trade" db:"last_trade" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	High      string `json:"high" db:"high" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Low       string `json:"low" db:"low" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Avg       string `json:"avg" db:"avg" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Vol       string `json:"vol" db:"vol" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	VolCurr   string `json:"vol_curr" db:"vol_curr" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Updated   int    `json:"updated" db:"update" db_type:"int" db_default:"not null" db_ops:"create,update"`
}

type WorkerMaxMinAvgoDto struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	High string `json:"high" db:"high" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Low  string `json:"low" db:"low" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Avg  string `json:"avg" db:"avg" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

type SellDto struct {
	Name      string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	SellPrice string `json:"sell_price" db:"sell_price" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

func (w *SellDto) OnCreate() []string {
	return []string{}
}

func (w *SellDto) TableName() string {
	return "SellDto"
}

type WorkerMinPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Low  string `json:"low" db:"low" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}
type WorkerMaxPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	High string `json:"high" db:"high" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}
type WorkerAvgPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Avg  string `json:"avg" db:"avg" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

func (w *WorkerMaxMinAvgoDto) TableName() string {
	return "WorkerMaxMinAvgoDto"
}

func (w *WorkerMaxMinAvgoDto) OnCreate() []string {
	return []string{}
}

func (q *WorkerDTO) TableName() string {
	return "workerDTO"
}

func (q *WorkerDTO) OnCreate() []string {
	return []string{}
}
