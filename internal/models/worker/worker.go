package worker

type Worker struct {
	name       string
	buy_price  float32
	sell_price float32
	high       float32
	low        float32
	avg        float32
}
