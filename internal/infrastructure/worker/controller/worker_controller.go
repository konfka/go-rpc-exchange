package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/component"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/errors"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/responder"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/service"
	"net/http"
)

type Workerer interface {
	MinPrices(w http.ResponseWriter, r *http.Request)
	MaxPrices(w http.ResponseWriter, r *http.Request)
	AveragePrices(w http.ResponseWriter, r *http.Request)
	History(w http.ResponseWriter, r *http.Request)
}

type Worker struct {
	service service.Workerer
	responder.Responder
	godecoder.Decoder
}

func (w2 *Worker) History(w http.ResponseWriter, r *http.Request) {
	res := w2.service.History(r.Context())
	if res.ErrorCode != errors.NoError {
		w2.OutputJSON(w, ProfileResponseHistory{
			ErrorCode: res.ErrorCode,
			Data:      DataHistory{Message: "no work history"},
		})
		return
	}
	w2.OutputJSON(w, ProfileResponseHistory{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataHistory{Worker: res.History},
	})
}

func (w2 *Worker) MinPrices(w http.ResponseWriter, r *http.Request) {
	res := w2.service.MinPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		w2.OutputJSON(w, ProfileResponseMin{
			ErrorCode: res.ErrorCode,
			Data:      DataMin{Message: "no work min"},
		})
		return
	}
	w2.OutputJSON(w, ProfileResponseMin{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataMin{Worker: res.Worker},
	})

}

func (w2 *Worker) MaxPrices(w http.ResponseWriter, r *http.Request) {
	res := w2.service.MaxPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		w2.OutputJSON(w, ProfileResponseMax{
			ErrorCode: res.ErrorCode,
			Data:      DataMax{Message: "no work"},
		})
		return
	}
	w2.OutputJSON(w, ProfileResponseMax{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataMax{Worker: res.Worker},
	})
}

func (w2 *Worker) AveragePrices(w http.ResponseWriter, r *http.Request) {
	res := w2.service.AvgPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		w2.OutputJSON(w, ProfileResponseAvg{
			ErrorCode: res.ErrorCode,
			Data:      DataAvg{Message: "no work"},
		})
		return
	}
	w2.OutputJSON(w, ProfileResponseAvg{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataAvg{Worker: res.Worker},
	})
}

func NewWorker(service service.Workerer, components *component.Components) Workerer {
	return &Worker{service: service, Responder: components.Responder, Decoder: components.Decoder}
}
