package service

import (
	"context"
	"gitlab.com/konfka/go-rpc-exchange/internal/models/worker"
)

type Workerer interface {
	TickerWork(model worker.WorkerDTO)
	MaxMinAvg(model worker.WorkerMaxMinAvgoDto)
	MinPrices(ctx context.Context) WorkerMinPriceOut
	MaxPrices(ctx context.Context) WorkerMaxPriceOut
	AvgPrices(ctx context.Context) WorkerAvgPriceOut
	History(ctx context.Context) WorkerHistory
	SellDto(model worker.SellDto)
}

type WorkerMinPriceOut struct {
	Worker    []worker.WorkerMinPrice
	ErrorCode int
	Succses   bool
}

type WorkerMaxPriceOut struct {
	Worker    []worker.WorkerMaxPrice
	ErrorCode int
	Succses   bool
}

type WorkerAvgPriceOut struct {
	Worker    []worker.WorkerAvgPrice
	ErrorCode int
	Succses   bool
}

type WorkerHistory struct {
	History   []worker.WorkerDTO
	ErrorCode int
	Succses   bool
}
