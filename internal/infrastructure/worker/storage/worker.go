package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/konfka/go-rpc-exchange/internal/db/adapter"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/db/scanner"
	"gitlab.com/konfka/go-rpc-exchange/internal/models/worker"
	"gitlab.com/konfka/go-rpc-exchange/internal/telegram"
	"strconv"
)

type WorkerStorage struct {
	adapter *adapter.SQLAdapter
}

func (s *WorkerStorage) Sell(ctx context.Context, models worker.SellDto) error {
	dto := []worker.SellDto{}
	search := []worker.SellDto{}
	_ = s.adapter.List(ctx, &dto, models.TableName(), adapter.Condition{Equal: sq.Eq{
		"name": models.Name,
	}})
	if len(dto) < 1 {
		err := s.adapter.Create(ctx, &models)
		if err != nil {
			return err
		}
	} else {
		_ = s.adapter.List(ctx, &search, models.TableName(), adapter.Condition{Equal: sq.Eq{
			"name": models.Name,
		}})
		intSearch, _ := strconv.Atoi(search[0].SellPrice)
		intModel, _ := strconv.Atoi(models.SellPrice)
		if intSearch-intModel > 100 || intSearch-intModel <= -100 {
			telegram.RabbitStart(search[0])
			err := s.adapter.Update(ctx, &models, adapter.Condition{
				Equal: sq.Eq{
					"name": models.Name,
				},
			}, scanner.Update)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *WorkerStorage) MaxPrice(ctx context.Context) ([]worker.WorkerMaxPrice, error) {
	dto := []worker.WorkerMaxPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "high")
	if err != nil {
		return []worker.WorkerMaxPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) AvgPrice(ctx context.Context) ([]worker.WorkerAvgPrice, error) {
	dto := []worker.WorkerAvgPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "avg")
	if err != nil {
		return []worker.WorkerAvgPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) MinPrice(ctx context.Context) ([]worker.WorkerMinPrice, error) {
	dto := []worker.WorkerMinPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "low")
	if err != nil {
		return []worker.WorkerMinPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) MaxMinAvg(ctx context.Context, models worker.WorkerMaxMinAvgoDto) (int, error) {
	dto := []worker.WorkerMaxMinAvgoDto{}
	err := s.adapter.List(ctx, &dto, models.TableName(), adapter.Condition{Equal: sq.Eq{
		"name": models.Name,
	}})
	if len(dto) < 1 {
		err = s.adapter.Create(ctx, &models)
		if err != nil {
			return 0, err
		}
	} else {
		err = s.adapter.Update(ctx, &models, adapter.Condition{
			Equal: sq.Eq{
				"name": models.Name,
			},
		}, scanner.Update)
		if err != nil {
			return 0, err
		}
	}
	return 1, err
}

func (s *WorkerStorage) History(ctx context.Context) ([]worker.WorkerDTO, error) {
	dto := []worker.WorkerDTO{}
	err := s.adapter.List(ctx, &dto, "workerDTO", adapter.Condition{})
	if err != nil {
		return []worker.WorkerDTO{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) TickerWork(ctx context.Context, model worker.WorkerDTO) (int, error) {
	err := s.adapter.Create(ctx, &model)
	return 0, err
}

func NewWorkerStorage(sqlAdapter *adapter.SQLAdapter) Workerer {
	return &WorkerStorage{adapter: sqlAdapter}
}

func (s *WorkerStorage) Create(ctx context.Context, u worker.WorkerDTO) (int, error) {
	err := s.adapter.Create(ctx, &u)
	return 0, err
}
