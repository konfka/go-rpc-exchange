package storage

import (
	"context"
	"gitlab.com/konfka/go-rpc-exchange/internal/models/worker"
)

type Workerer interface {
	TickerWork(ctx context.Context, model worker.WorkerDTO) (int, error)
	MaxMinAvg(ctx context.Context, model worker.WorkerMaxMinAvgoDto) (int, error)
	MinPrice(ctx context.Context) ([]worker.WorkerMinPrice, error)
	MaxPrice(ctx context.Context) ([]worker.WorkerMaxPrice, error)
	AvgPrice(ctx context.Context) ([]worker.WorkerAvgPrice, error)
	History(ctx context.Context) ([]worker.WorkerDTO, error)
	Sell(ctx context.Context, dto worker.SellDto) error
}
