package worker

import (
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/component"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/controller"
)

type Controllers struct {
	Worker controller.Workerer
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	workerController := controller.NewWorker(services.Worker, components)

	return &Controllers{
		Worker: workerController,
	}
}
