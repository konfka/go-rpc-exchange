package worker

import (
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/component"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/service"
	"gitlab.com/konfka/go-rpc-exchange/internal/storages"
)

type Services struct {
	Worker service.Workerer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{

		Worker: service.NewWorkerService(storages.Worker, components.Logger, components),
	}
}
