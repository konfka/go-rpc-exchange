package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/konfka/go-rpc-exchange/config"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/responder"
	"go.uber.org/zap"
)

type Components struct {
	Conf      config.AppConf
	Responder responder.Responder
	Decoder   godecoder.Decoder
	Logger    *zap.Logger
}

func NewComponents(conf config.AppConf, responder responder.Responder, decoder godecoder.Decoder, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Responder: responder, Decoder: decoder, Logger: logger}
}
