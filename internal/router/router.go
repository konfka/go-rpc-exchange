package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/component"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker"
	"net/http"
)

func NewApiRouter(controllers *worker.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	return r
}
