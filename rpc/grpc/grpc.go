package grpc

import (
	"context"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/service"
	model "gitlab.com/konfka/go-rpc-exchange/internal/models/worker"
	"gitlab.com/konfka/go-rpc-exchange/rpc/grpc/proto"
)

type WorkerServiceJSONGRPC struct {
	worker service.Workerer
}

func (w WorkerServiceJSONGRPC) History(ctx context.Context, request *proto.BaseRequest) (*proto.ProfileResponseHistory, error) {
	out := w.worker.History(ctx)
	res := w.modelToExchangeValueHistory(out.History)
	return &proto.ProfileResponseHistory{Success: out.Succses, ErrorCode: int32(out.ErrorCode), WorkerDtos: res}, nil
}

func (w WorkerServiceJSONGRPC) Max(ctx context.Context, request *proto.BaseRequest) (*proto.ProfileResponseMax, error) {
	out := w.worker.MaxPrices(ctx)
	res := w.modelToExchangeValueMax(out.Worker)
	return &proto.ProfileResponseMax{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Coins: res}, nil

}

func (w WorkerServiceJSONGRPC) Min(ctx context.Context, request *proto.BaseRequest) (*proto.ProfileResponseMin, error) {
	out := w.worker.MinPrices(ctx)
	res := w.modelToExchangeValueMin(out.Worker)
	return &proto.ProfileResponseMin{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Coins: res}, nil

}

func (w WorkerServiceJSONGRPC) Avg(ctx context.Context, request *proto.BaseRequest) (*proto.ProfileResponseAvg, error) {
	out := w.worker.AvgPrices(ctx)
	res := w.modelToExchangeValueAvg(out.Worker)
	return &proto.ProfileResponseAvg{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Coins: res}, nil

}

func (w WorkerServiceJSONGRPC) mustEmbedUnimplementedExchangeServiceRPCServer() {
	//TODO implement me
	panic("implement me")
}

func NewWorkerServiceJSONGRPC(worker service.Workerer) *WorkerServiceJSONGRPC {
	return &WorkerServiceJSONGRPC{worker: worker}
}

func (w WorkerServiceJSONGRPC) modelToExchangeValueHistory(in []model.WorkerDTO) []*proto.WorkerDto {
	var workerDtos []*proto.WorkerDto
	for _, i := range in {
		workerDto := &proto.WorkerDto{
			Id:        int64(i.ID),
			Name:      i.Name,
			BuyPrice:  i.BuyPrice,
			SellPrice: i.SellPrice,
			LastTrade: i.LastTrade,
			High:      i.High,
			Low:       i.Low,
			Avg:       i.Avg,
			VolCurr:   i.VolCurr,
			Updated:   int64(i.Updated),
		}
		workerDtos = append(workerDtos, workerDto)
	}
	return workerDtos
}

func (w WorkerServiceJSONGRPC) modelToExchangeValueMax(in []model.WorkerMaxPrice) []*proto.ValueMax {
	var workerDtos []*proto.ValueMax
	for _, i := range in {
		workerDto := &proto.ValueMax{
			Name: i.Name,
			High: i.High,
		}
		workerDtos = append(workerDtos, workerDto)
	}
	return workerDtos
}

func (w WorkerServiceJSONGRPC) modelToExchangeValueMin(in []model.WorkerMinPrice) []*proto.ValueMin {
	var workerDtos []*proto.ValueMin
	for _, i := range in {
		workerDto := &proto.ValueMin{
			Name: i.Name,
			Low:  i.Low,
		}
		workerDtos = append(workerDtos, workerDto)
	}
	return workerDtos
}

func (w WorkerServiceJSONGRPC) modelToExchangeValueAvg(in []model.WorkerAvgPrice) []*proto.ValueAvg {
	var workerDtos []*proto.ValueAvg
	for _, i := range in {
		workerDto := &proto.ValueAvg{
			Name: i.Name,
			Avg:  i.Avg,
		}
		workerDtos = append(workerDtos, workerDto)
	}
	return workerDtos
}
