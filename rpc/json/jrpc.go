package json

import (
	"context"
	"gitlab.com/konfka/go-rpc-exchange/internal/infrastructure/worker/service"
)

type WorkerServerJSONRPC struct {
	workerRPC service.Workerer
}

type Empty struct {
}

func NewWorkerServerJSONRPC(workerRPC service.Workerer) *WorkerServerJSONRPC {
	return &WorkerServerJSONRPC{workerRPC: workerRPC}
}

func (t *WorkerServerJSONRPC) History(in Empty, out *service.WorkerHistory) error {
	*out = t.workerRPC.History(context.Background())
	return nil
}

func (t *WorkerServerJSONRPC) MaxPrice(in Empty, out *service.WorkerMaxPriceOut) error {
	*out = t.workerRPC.MaxPrices(context.Background())
	return nil
}

func (t *WorkerServerJSONRPC) MinPrice(in Empty, out *service.WorkerMinPriceOut) error {
	*out = t.workerRPC.MinPrices(context.Background())
	return nil
}

func (t *WorkerServerJSONRPC) AvgPrice(in Empty, out *service.WorkerAvgPriceOut) error {
	*out = t.workerRPC.AvgPrices(context.Background())
	return nil
}
